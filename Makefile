CC_OPTION := -Wformat -Wformat-security -Werror=format-security -Werror=write-strings -Werror -Werror=format

TARGET  := hdmirx_tool
OBJECTS := \
	src/hdmirx_tool.o \
	lib/hdmi_ctl.o \
	lib/hdmi_event.o \
	utils/mutex.o \
	utils/thread.o \

$(OBJECTS): CPPFLAGS := $(CC_OPTION) -Iinc/ -Iutils/

.PHONY: all clean install

all: $(TARGET)
$(TARGET): $(OBJECTS)
	$(LINK.o) $^ $(LOADLIBES) -pthread -ldl $(LDLIBS) -o $@

clean:
	$(RM) $(OBJECTS) $(TARGET)

install: $(TARGET)
	install -d $(DESTDIR)$(bindir)
	install -m 755 $(TARGET) $(DESTDIR)$(bindir)
