#include <inttypes.h>
#include <malloc.h>
#include <cstdint>

#include "hdmi_if.h"


#define DUMP(a)                  \
    do {                         \
        printf(#a " = %d\n", a); \
    } while (0)

static void print_cmd(void) {
    printf("1) enable hdmi      2) disable hdmi\n");
    printf("3) get device info  4) check cable\n");
    printf("5) get video info   6) check video locked\n");
    printf("7) get audio info   8) check audio locked\n");
    printf("9) set test edid\n");
    printf("a) start observing  b) stop observing\n");
    printf("h) help             q) quit\n");
}

/*****************************************************************************/

static void get_video_info(void) {
    struct HDMIRX_VID_PARA v;
    if (hdmi_get_video_info(&v) < 0) return;

    DUMP(v.cs);
    DUMP(v.dp);
    DUMP(v.htotal);
    DUMP(v.vtotal);
    DUMP(v.hactive);
    DUMP(v.vactive);
    DUMP(v.is_pscan);
    DUMP(v.hdmi_mode);
    DUMP(v.frame_rate);
    DUMP(v.pixclk);
}

static void get_audio_info(void) {
    struct HDMIRX_AUD_INFO a;
    uint8_t sp, wl;

    if (hdmi_get_audio_info(&a) < 0)
        return;

    DUMP(a.info.is_HBRAudio);
    DUMP(a.info.is_DSDAudio);
    DUMP(a.info.is_RawSDAudio);
    DUMP(a.info.is_PCMMultiCh);
    DUMP(a.caps.SampleFreq);
    DUMP(a.caps.AudInf.info.AudioChannelCount);
    DUMP(a.caps.AudInf.info.SpeakerPlacement);
    DUMP(a.caps.AudChStat.WordLen);

    wl = a.caps.AudChStat.WordLen;

    /* IEC 60958-3 */
    uint8_t max_bits_flag = wl & 0x1; // presenting bits. 0 presents max 20 bits, 1 is max 24 bits;
    printf("Audio Bits:\n");
    if (max_bits_flag == 0) {   // max 20 bits mode
       switch ((wl & 0xE) >> 1) {
           case (0x1): printf("  16 bits\n"); break;
           case (0x2): printf("  17 bits\n"); break;
           case (0x3): printf("  18 bits\n"); break;
           case (0x4): printf("  19 bits\n"); break;
           case (0x5): printf("  20 bits\n"); break;
           case (0x0): printf("  not indicated (default)\n"); break;
           default:    printf("  reserved\n");break;
       }
    } else {                    // max 24 bits mode
        switch ((wl & 0xE) >> 1) {
           case (0x1): printf("  20 bits\n"); break;
           case (0x2): printf("  21 bits\n"); break;
           case (0x3): printf("  22 bits\n"); break;
           case (0x4): printf("  23 bits\n"); break;
           case (0x5): printf("  24 bits\n"); break;
           case (0x0): printf("  not indicated (default)\n"); break;
           default:    printf("  reserved\n");break;
       }
    }

    printf("Audio Channel Info:\n");
    /* CEA-861-E, 6.6.1 */
    if (a.caps.AudInf.info.AudioChannelCount)
        printf("  Channel Number [%u]\n", (a.caps.AudInf.info.AudioChannelCount + 1));
    else
        printf("  Channel Number by Stream Header\n");
    /* CEA-861-E, 6.6.2 */
    sp = a.caps.AudInf.info.SpeakerPlacement;
    printf("  Speaker Placement [0x%x]\n", sp);  // double check CH number with speaker replacement

    // check sampling rate
    printf("Audio Sample Freq:\n");
    switch (a.caps.SampleFreq) {
        case HDMI2_AUD_FS_32K:
            printf("  32.0 kHz\n"); break;
        case HDMI2_AUD_FS_44K:
            printf("  44.1 kHz\n"); break;
        case HDMI2_AUD_FS_48K:
            printf("  48.0 kHz\n"); break;
        case HDMI2_AUD_FS_96K:
            printf("  96.0 kHz\n"); break;
        case HDMI2_AUD_FS_192K:
            printf(" 192.0 kHz\n"); break;
        default:
            printf("  Please mapping [%d] to HDMI2_AUD_FS manually..\n", a.caps.SampleFreq); break;
    }
}

static void get_device_info(void) {
    struct HDMIRX_DEV_INFO d;
    if (hdmi_get_device_info(&d) < 0) return;

    DUMP(d.hdmirx5v);
    DUMP(d.hpd);
    DUMP(d.power_on);
    DUMP(d.vid_locked);
    DUMP(d.aud_locked);
    DUMP(d.hdcp_version);
}

static void check_cable(void) {
    if (hdmi_check_cable())
        printf("hdmi connected\n");
    else
        printf("hdmi disconnected\n");
}

static void check_video_locked(void) {
    if (hdmi_check_video_locked())
        printf("video locked\n");
    else
        printf("video unlocked\n");
}

static void check_audio_locked(void) {
    if (hdmi_check_audio_locked())
        printf("audio locked\n");
    else
        printf("audio unlocked\n");
}
/*****************************************************************************/

int main(void) {
    char ch;
    struct HDMIRX_DRIVER_VERSION ver = {0};

    hdmi_get_driver_version(&ver);
    printf("hdmirx tool version:   %u.%u.%u\n", HDMIRX_DRV_VER_MAJOR, HDMIRX_DRV_VER_MINOR, HDMIRX_DRV_VER_PATCHLEVEL);
    printf("hdmirx driver version: %u.%u.%u\n\n", ver.version_major, ver.version_minor, ver.version_patchlevel);

    print_cmd();

    do {
        ch = getchar();
        printf("getchar=%c\n", ch);
        switch (ch) {
        case '1': hdmi_enable(1);         break;
        case '2': hdmi_enable(0);         break;
        case '3': get_device_info();      break;
        case '4': check_cable();          break;
        case '5': get_video_info();       break;
        case '6': check_video_locked();   break;
        case '7': get_audio_info();       break;
        case '8': check_audio_locked();   break;
        case '9': hdmi_set_test_edid();   break;
        case 'a': hdmi_start_observing(); break;
        case 'q': [[fallthrough]];
        case 'b': hdmi_stop_observing();  break;
        case 'h': print_cmd();            break;
        default:                          break;
        }
    } while (ch != 'q');
    return 0;
}
