#ifndef __KERNEL_THREAD_H
#define __KERNEL_THREAD_H

#include <pthread.h>

#define NO_ERROR 0
#define INFINITE_TIME (0xffffffff)

typedef int status_t;

enum thread_id {
	THREAD_APP = 0,
	/*...*/
	NR_THREAD,
};

typedef int (*thread_start_routine)(void *arg);

typedef struct thread thread_t;

/* thread priority */
#define NUM_PRIORITIES 32
#define LOWEST_PRIORITY 0
#define HIGHEST_PRIORITY (NUM_PRIORITIES - 1)
#define DPC_PRIORITY (NUM_PRIORITIES - 2)
#define IDLE_PRIORITY (LOWEST_PRIORITY + 1)
#define NEVER_RUN_PRIORITY LOWEST_PRIORITY
#define LOW_PRIORITY (NUM_PRIORITIES / 4)
#define DEFAULT_PRIORITY (NUM_PRIORITIES / 2)
#define HIGH_PRIORITY ((NUM_PRIORITIES / 4) * 3)

#ifdef __cplusplus
extern "C" {
#endif

thread_t *thread_create(const unsigned int id, const char *name, thread_start_routine entry, void *arg, unsigned int priority);
status_t thread_resume(thread_t *);
int thread_exit(const unsigned int id);

#ifdef __cplusplus
}
#endif


#endif // __KERNEL_THREAD_H