#include <sys/prctl.h>
#include <assert.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include "thread.h"
#include "err.h"


/*
thread_create
thread_resume
*/

struct thread {
	pthread_t thread;
	pthread_mutex_t mutex;
	pthread_attr_t attr;
	/* entry point */
	thread_start_routine entry;
	void *arg;

	char name[16];
};

static thread_t threads[NR_THREAD];

static void thread_suspend(thread_t *thread)
{
	while (pthread_mutex_lock(&thread->mutex) != 0);
}

status_t thread_resume(thread_t *thread)
{
	while (pthread_mutex_unlock(&thread->mutex) != 0);
	return NO_ERROR;
}

static void *thread_entry(void *arg)
{
	thread_t *thread = (thread_t *)(arg);

	prctl(PR_SET_NAME, thread->name);

	thread_suspend(thread);
	thread->entry(thread->arg);

	thread->thread = (pthread_t)(0);
	return NULL;
}

thread_t *thread_create(const unsigned int id, const char *name, thread_start_routine entry, void *arg, unsigned int priority)
{
	thread_t *thread = &threads[id];
	int i;

	pthread_mutex_init(&thread->mutex, NULL);
	pthread_mutex_lock(&thread->mutex);

	strncpy(thread->name, name, sizeof(thread->name));
	thread->name[sizeof(thread->name)-1] = '\0';
	thread->entry = entry;
	thread->arg = arg;

	pthread_attr_init(&thread->attr);
	pthread_attr_setdetachstate(&thread->attr, PTHREAD_CREATE_JOINABLE);
	i = pthread_create(&thread->thread, &thread->attr, thread_entry, thread);
	pthread_attr_destroy(&thread->attr);
	assert(i == 0);

	return thread;
}

int thread_exit(const unsigned int id)
{
	thread_t *thread = &threads[id];
	int ret = 0;
	if (thread->thread)
        ret = pthread_cancel(thread->thread);
	return ret;
}
