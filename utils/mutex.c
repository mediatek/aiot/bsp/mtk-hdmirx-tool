#include <err.h>
#include <pthread.h>
#include <time.h>
#include "mutex.h"
#include "thread.h"

/**
 * @brief  Initialize a mutex_t
 */
void mutex_init(mutex_t *m)
{
	pthread_mutex_init(&m->mux, NULL);
}

/**
 * @brief  Destroy a mutex_t
 *
 * This function frees any resources that were allocated
 * in mutex_init().  The mutex_t object itself is not freed.
 */
void mutex_destroy(mutex_t *m)
{
	pthread_mutex_destroy(&m->mux);
}

/**
 * @brief  Acquire a mutex; wait if needed.
 *
 * This function waits for a mutex to become available.  It
 * may wait forever if the mutex never becomes free.
 *
 * @return  NO_ERROR on success, other values on error
 */
status_t mutex_acquire(mutex_t *m)
{
	return pthread_mutex_lock(&m->mux);
}

/**
 * @brief  Release mutex
 */
status_t mutex_release(mutex_t *m)
{
	return pthread_mutex_unlock(&m->mux);
}
