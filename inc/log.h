#ifndef LIB_INC_LOG_H__
#define LIB_INC_LOG_H__

#include <cstdio>

/* /< show information log */
#define ALOGV(x, ...) do { \
    printf(x, ##__VA_ARGS__); printf("\n"); }while(0);

#define ALOGE(x, ...) do { \
    printf(x, ##__VA_ARGS__); printf("\n"); }while(0);

#define ALOGD(x, ...) do { \
    printf(x, ##__VA_ARGS__); printf("\n"); }while(0);

#define ALOGI(x, ...) do { \
    printf(x, ##__VA_ARGS__); printf("\n"); }while(0);

#endif  // LIB_INC_LOG_H__
