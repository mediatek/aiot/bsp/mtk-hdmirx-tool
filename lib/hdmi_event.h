#ifndef LIB_HDMI_EVENT_H__
#define LIB_HDMI_EVENT_H__

#include "thread.h"
#include "mutex.h"


// ---------------------------------------------------------------------------
typedef struct _uevent_hdlr_t {
	mutex_t m_lock;
    int m_socket;
    thread_t *m_thread;
} uevent_hdlr_t;

uevent_hdlr_t* hdmi_uevent_hdlr_create();
void hdmi_uevent_hdlr_init(uevent_hdlr_t* hdlr);
void hdmi_uevent_hdlr_run(uevent_hdlr_t* hdlr);
void hdmi_uevent_hdlr_stop(uevent_hdlr_t* hdlr);
void hdmi_uevent_hdlr_deinit(uevent_hdlr_t* hdlr);
void hdmi_uevent_hdlr_destroy(uevent_hdlr_t* hdlr);

#endif  // LIB_HDMI_EVENT_H__
