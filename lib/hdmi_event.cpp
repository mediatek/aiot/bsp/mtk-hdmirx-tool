
#include "hdmi_event.h"

#include <linux/netlink.h>
#include <poll.h>
#include <sys/socket.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cerrno>
#include <cstdint>
#include <cstring>
#include "log.h"
#include "thread.h"
#include "mutex.h"
#include "hdmi_if.h"

#define UEVENT_BUF_SIZE 2048

/*****************************************************************************/
uevent_hdlr_t* hdmi_uevent_hdlr_create() {
    uevent_hdlr_t* ctx;

    ctx = (uevent_hdlr_t*)malloc(sizeof(uevent_hdlr_t));
    if (ctx == nullptr) {
        ALOGE("Unable to create uevent hdlr!");
        return NULL;
    }
    memset(ctx, 0, sizeof(*ctx));

    // initialize
    ctx->m_socket = -1;
    mutex_init(&ctx->m_lock);

    return ctx;
}

void hdmi_uevent_hdlr_init(uevent_hdlr_t* hdlr) {
    uevent_hdlr_t* ctx = hdlr;

    ALOGV("%s@%d", __FUNCTION__, __LINE__);
    struct sockaddr_nl addr_sock;
    int optval = 64 * 1024;

    ctx->m_socket = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (ctx->m_socket < 0) {
        ALOGE("Unable to create uevent socket:%s", strerror(errno));
        return;
    }
    if (setsockopt(ctx->m_socket, SOL_SOCKET, SO_RCVBUFFORCE, &optval,
                   sizeof(optval)) < 0) {
        ALOGE("Unable to set uevent socket SO_RCVBUF option:%s(%d)",
              strerror(errno), errno);
    }

    memset(&addr_sock, 0, sizeof(addr_sock));
    addr_sock.nl_family = AF_NETLINK;
    // addr_sock.nl_pad = 0xcec;
    addr_sock.nl_pid = getpid() << 3;
    addr_sock.nl_groups = 0xffffffff;

    ALOGI("Start to initialize, nl_pid(%d)", addr_sock.nl_pid);
    if (bind(ctx->m_socket, (struct sockaddr *)&addr_sock, sizeof(addr_sock)) < 0) {
        ALOGE("Failed to bind socket:%s(%d)", strerror(errno), errno);
        close(ctx->m_socket);
        ctx->m_socket = -1;
        return;
    }
}

static void _hdmi_uevent_hdlr_handle_uevnets(const char *buff, int len) {
    const char *s = buff;
    int notify = 0;

    if (strcmp(s, "change@/devices/virtual/hdmirxswitch/hdmi"))
        return;

    s += strnlen(s, UEVENT_BUF_SIZE - 1) + 1;
    while (*s) {
        if (!strncmp(s, "SWITCH_NOTIFY=", strlen("SWITCH_NOTIFY="))) {
            notify = atoi(s + strlen("SWITCH_NOTIFY="));
            //ALOGD("uevents: SWITCH_NOTIFY=%d", notify);
        }
        ALOGV("uevents: s=%p, %s", s, s);
        s += strnlen(s, UEVENT_BUF_SIZE - 1) + 1;
        if (s - buff >= len) break;
    }

    // add callback if needed
    switch (notify) {
    case HDMI_RX_PWR_5V_CHANGE:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_PWR_5V_CHANGE)", notify);
        break;
    case HDMI_RX_TIMING_LOCK:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_TIMING_LOCK)", notify);
        break;
    case HDMI_RX_TIMING_UNLOCK:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_TIMING_UNLOCK)", notify);
        break;
    case HDMI_RX_AUD_LOCK:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_AUD_LOCK)", notify);
        break;
    case HDMI_RX_AUD_UNLOCK:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_AUD_UNLOCK)", notify);
        break;
    case HDMI_RX_PLUG_IN:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_PLUG_IN)", notify);
        break;
    case HDMI_RX_PLUG_OUT:
        //if (_tv_cb) (*_tv_cb)(_dev);
        ALOGV("RX notify: %d (HDMI_RX_PLUG_OUT)", notify);
        break;
    default:
        ALOGV("RX notify: %d", notify);
        break;
    }
}

static int _hdmi_uevent_hdlr_thread_loop(void *arg) {
    uevent_hdlr_t* ctx = (uevent_hdlr_t *)arg;
    struct pollfd fds;
    static char uevent_desc[UEVENT_BUF_SIZE * 2];

    while (ctx->m_socket > 0) {
        mutex_acquire(&ctx->m_lock);

        fds.fd = ctx->m_socket;
        fds.events = POLLIN;
        fds.revents = 0;
        int ret = poll(&fds, 1, -1);

        if (ret > 0 && (fds.revents & POLLIN)) {
            /* keep last 2 zeroes to ensure double 0 termination */
            int count = recv(ctx->m_socket, uevent_desc, sizeof(uevent_desc) - 2, 0);

            if (count > 0)
                _hdmi_uevent_hdlr_handle_uevnets(uevent_desc, count);
        }

        mutex_release(&ctx->m_lock);
    }

    return true;
}

void hdmi_uevent_hdlr_run(uevent_hdlr_t* hdlr) {
    uevent_hdlr_t* ctx = hdlr;

    ALOGD("hdmi_uevent_hdlr_run..");

    ctx->m_thread = thread_create(THREAD_APP, "HDMIRX_UEVENT_HDLR", _hdmi_uevent_hdlr_thread_loop, (void*)ctx, DEFAULT_PRIORITY);
    thread_resume(ctx->m_thread);

    ALOGD("hdmi_uevent_hdlr_run.. done");
}

void hdmi_uevent_hdlr_stop(uevent_hdlr_t* hdlr) {
    uevent_hdlr_t* ctx = hdlr;

    ALOGD("hdmi_uevent_hdlr_stop..");
    thread_exit(THREAD_APP);
    ALOGD("hdmi_uevent_hdlr_stop.. done");
}

void hdmi_uevent_hdlr_deinit(uevent_hdlr_t* hdlr) {
    uevent_hdlr_t* ctx = hdlr;

    ALOGD("hdmi_uevent_hdlr_deinit");
    if (ctx->m_socket > 0) {
        close(ctx->m_socket);
        ctx->m_socket = -1;
    }
}

void hdmi_uevent_hdlr_destroy(uevent_hdlr_t* hdlr) {
    uevent_hdlr_t* ctx = hdlr;

    ALOGD("hdmi_uevent_hdlr_destroy");
    mutex_destroy(&ctx->m_lock);

    free(ctx);
}
