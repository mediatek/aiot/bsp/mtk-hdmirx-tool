
#include <fcntl.h>
//#include <log/log.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdint>
#include <errno.h>

#include "hdmi_event.h"
#include "hdmi_if.h"
#include "log.h"

/*****************************************************************************/

const struct HDMIRX_EDID_DATA cts_4k_edid = {
        {0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,
         0x41, 0x0C, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01,
         0x0F, 0x1A, 0x01, 0x03, 0x80, 0x80, 0x48, 0x78,
         0x0A, 0xE6, 0x92, 0xA3, 0x54, 0x4A, 0x99, 0x26,
         0x0F, 0x4A, 0x4C, 0x21, 0x08, 0x00, 0x81, 0xC0,
         0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
         0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x08, 0xE8,
         0x00, 0x30, 0xF2, 0x70, 0x5A, 0x80, 0xB0, 0x58,
         0x8A, 0x00, 0x80, 0x68, 0x21, 0x00, 0x00, 0x1E,
         0x02, 0x3A, 0x80, 0x18, 0x71, 0x38, 0x2D, 0x40,
         0x58, 0x2C, 0x45, 0x00, 0x80, 0x68, 0x21, 0x00,
         0x00, 0x1E, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x50,
         0x68, 0x69, 0x6C, 0x69, 0x70, 0x73, 0x20, 0x46,
         0x54, 0x56, 0x0A, 0x20, 0x00, 0x00, 0x00, 0xFD,
         0x00, 0x30, 0x3E, 0x0F, 0x8C, 0x3C, 0x00, 0x0A,
         0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x01, 0x1E,
         0x02, 0x03, 0x4B, 0xF1, 0x59, 0x60, 0x61, 0x65,
         0x66, 0x5D, 0x5F, 0x5E, 0x10, 0x1F, 0x20, 0x22,
         0x21, 0x05, 0x14, 0x04, 0x13, 0x12, 0x03, 0x11,
         0x02, 0x16, 0x07, 0x15, 0x06, 0x01, 0x2C, 0x09,
         0x1F, 0x07, 0x11, 0x07, 0x50, 0x39, 0x07, 0xFF,
         0x51, 0x07, 0x00, 0x83, 0x01, 0x00, 0x00, 0x6F,
         0x03, 0x0C, 0x00, 0x10, 0x00, 0x78, 0x78, 0xA0,
         0x4C, 0x4C, 0x00, 0x60, 0x01, 0x02, 0x03, 0x67,
         0xD8, 0x5D, 0xC4, 0x01, 0x78, 0xC8, 0x07, 0xE2,
         0x00, 0x49, 0xE2, 0x0F, 0x0F, 0x01, 0x1D, 0x80,
         0x3E, 0x73, 0x38, 0x2D, 0x40, 0x7E, 0x2C, 0x45,
         0x80, 0x80, 0x68, 0x21, 0x00, 0x00, 0x1E, 0x01,
         0x1D, 0x80, 0xD0, 0x72, 0x1C, 0x16, 0x20, 0x10,
         0x2C, 0x25, 0x80, 0x80, 0x68, 0x21, 0x00, 0x00,
         0x9E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x8C}
};

/* get driver version, return < 0 means error */
int hdmi_get_driver_version(struct HDMIRX_DRIVER_VERSION *ver) {
    //ALOGV("%s", __FUNCTION__);
    int fd = open(HDMIRX_DEV_PATH, O_WRONLY);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_DRV_VER, ver);
    close(fd);
    return ret;
}

/* 1: enable, otherwise: disable */
int hdmi_enable(uint64_t en) {
    ALOGV("%s[%lu]", __FUNCTION__, en);
    int fd = open(HDMIRX_DEV_PATH, O_WRONLY);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_ENABLE, en);
    close(fd);
    return ret;
}

/* could be called only after video locked */
int hdmi_get_video_info(struct HDMIRX_VID_PARA *vinfo) {
    ALOGV("%s", __FUNCTION__);
    if (vinfo == nullptr) return -EINVAL;

    int fd = open(HDMIRX_DEV_PATH, O_RDWR);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_VID_INFO, vinfo);
    close(fd);
    return ret;
}

/* could be called only after audio locked */
int hdmi_get_audio_info(struct HDMIRX_AUD_INFO *ainfo) {
    ALOGV("%s", __FUNCTION__);
    if (ainfo == nullptr) return -EINVAL;

    int fd = open(HDMIRX_DEV_PATH, O_RDWR);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_AUD_INFO, ainfo);
    close(fd);
    return ret;
}

int hdmi_get_device_info(struct HDMIRX_DEV_INFO *dinfo) {
    ALOGV("%s", __FUNCTION__);
    if (dinfo == nullptr) return -EINVAL;

    int fd = open(HDMIRX_DEV_PATH, O_RDWR);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_DEV_INFO, dinfo);
    close(fd);
    return ret;
}

uint8_t hdmi_check_cable(void) {
    ALOGV("%s", __FUNCTION__);
    struct HDMIRX_DEV_INFO d;
    if (hdmi_get_device_info(&d)) return 0;
    return d.hdmirx5v & 0x1;
}

uint8_t hdmi_check_video_locked(void) {
    ALOGV("%s", __FUNCTION__);
    struct HDMIRX_DEV_INFO d;
    if (hdmi_get_device_info(&d)) return 0;
    return d.vid_locked;
}

uint8_t hdmi_check_audio_locked(void) {
    ALOGV("%s", __FUNCTION__);
    struct HDMIRX_DEV_INFO d;
    if (hdmi_get_device_info(&d)) return 0;
    return d.aud_locked;
}

/* 1: enable, otherwise: disable */
int hdmi_set_test_edid(void) {
    ALOGV("%s", __FUNCTION__);
    int fd = open(HDMIRX_DEV_PATH, O_WRONLY);
    if (fd < 0) {
        ALOGE("%s, open %s fail %d", __FUNCTION__, HDMIRX_DEV_PATH, fd);
        return fd;
    }
    int ret = ioctl(fd, MTK_HDMIRX_SET_EDID, &cts_4k_edid);
    close(fd);
    if (ret < 0) {
        ALOGE("%s, ioctl MTK_HDMIRX_SET_EDID fail %d", __FUNCTION__, ret);
    } else {
        int i = 0, j = 0;
        printf("set test edid: cts_4k_edid\n");

        for (i = 0, j = 0; i < HDMIRX_EDID_SIZE; i+=16, j++) {
             /* 128 bytes a block */
            if ((i % 128) == 0) {
                printf("EDID Block Number=#%d\n", i / 128);
                j = 0;
            }
            printf("0%d:%.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X", j,
                cts_4k_edid.data[i + 0], cts_4k_edid.data[i + 1],
                cts_4k_edid.data[i + 2], cts_4k_edid.data[i + 3],
                cts_4k_edid.data[i + 4], cts_4k_edid.data[i + 5],
                cts_4k_edid.data[i + 6], cts_4k_edid.data[i + 7]);
            printf(" %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X\n",
                cts_4k_edid.data[i + 8], cts_4k_edid.data[i + 9],
                cts_4k_edid.data[i +10], cts_4k_edid.data[i +11],
                cts_4k_edid.data[i +12], cts_4k_edid.data[i +13],
                cts_4k_edid.data[i +14], cts_4k_edid.data[i +15]);
        }
    }
    return ret;
}

/*****************************************************************************/

static uevent_hdlr_t *event_thread;

int hdmi_start_observing(void *dev, int (*tv_cb)(void *)) {
    ALOGV("%s", __FUNCTION__);
    event_thread = hdmi_uevent_hdlr_create();
    if (event_thread == nullptr) {
        ALOGE("create uevent thread fail");
        return -EINVAL;
    }
    hdmi_uevent_hdlr_init(event_thread);
    hdmi_uevent_hdlr_run(event_thread);

    return 0;
}

void hdmi_stop_observing(void) {
    if (event_thread) {
        ALOGV("%s", __FUNCTION__);
        hdmi_uevent_hdlr_stop(event_thread);
        hdmi_uevent_hdlr_deinit(event_thread);
        hdmi_uevent_hdlr_destroy(event_thread);
        event_thread = nullptr;
    }
}
